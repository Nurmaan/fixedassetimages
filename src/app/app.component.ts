import { Component, OnInit, ɵConsole, Injector } from '@angular/core';
import { VariableService } from './services/variable/variable.service';
export let InjectorInstance;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private variable: VariableService, private injector: Injector) {
    InjectorInstance = injector;
  }

  ngOnInit(): void {

    //disable loader image
    document.getElementById("loaderImage").style.display = "none";

    //set api url
    var config = document.getElementById("config");
    this.variable.setBaseApi(config.getAttribute("api"));
    this.variable.setUILink(config.getAttribute("ui"));
  }

}
