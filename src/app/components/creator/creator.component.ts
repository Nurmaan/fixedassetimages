import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api-service.service';
import { VariableService } from 'src/app/services/variable/variable.service';
import { MessageService } from 'src/app/services/message/message.service';
import { CSV } from './csv';
import { BaseReturnType } from 'src/app/services/variable/Model';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.css']
})
export class CreatorComponent implements OnInit {
  data = [];
  CSV: CSV;
  requests = 0;
  i = 1;
  assetType = "";
  error = [];
  completed = false;

  constructor(
    private apiService: ApiService,
    private variable: VariableService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.CSV = new CSV();
  }

  usersSelected(e) {
    this.i = 1;
    this.CSV.handleFiles(e.target.files);
    this.CSV.onDataProcessed = data => {
      this.data = data;
      if (this.assetType.length > 0) this.processData();
      else this.messageService.showWarning("Invalid Asset Type", "");
    }
  }

  processData() {
    console.log('processing', this.requests, this.i, this.data.length, this.i < this.data.length);
    if (this.i < this.data.length) {
      this.completed = false;
      for (var j = this.i; j < this.i + 30 && j < this.data.length; j++) {
        var datum = this.data[j];
        if (datum != "") {
          var assetCode = datum[this.data[0].indexOf("AssetCode")],
            imagePath = datum[this.data[0].indexOf("picturePat")].split("|")[0],
            pieces = imagePath.split("/");
          pieces.pop();
          imagePath = pieces.join("/");
          this.requests++;
          if (imagePath != "") {
            this.apiService.post(this.variable.FixedAsset, { function: "FixedAsset", assetCode: assetCode, picturePat: imagePath.replace("/", ""), assetType: this.assetType }).subscribe((data: BaseReturnType<string>) => {
              if (!data.success) {
                this.error.push({ assetCode: assetCode, picturePat: imagePath.replace("/", ""), assetType: this.assetType });
              }
              this.handleRequestResponse();
            });
          } else {
            this.handleRequestResponse();
          }

        } else {
          break;
        }
      }
    } else {
      this.completed = true;
    }
  }

  handleRequestResponse() {
    this.requests--;
    if (this.completed) {
      this.completed = false;
      this.messageService.showSuccess("Completed!", "All requests were sent");
      console.warn(this.error);
    } else {
      if (this.requests <= 0) {
        this.requests = 0;
        this.i += 30;
        if (this.i < this.data.length) this.processData();
      }
    }
  }


}
