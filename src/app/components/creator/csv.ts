
export class CSV {
    onDataProcessed = null;
    constructor() { }
    handleFiles(files) {
        this.getAsText(files[0]);
    }

    private getAsText(fileToRead) {
        console.log("get as text", fileToRead);
        var reader = new FileReader();
        // Read file into memory as UTF-8      
        reader.readAsText(fileToRead);
        // Handle errors load
        reader.onloadend = function (e) { this.loadHandler(e) }.bind(this);
        reader.onerror = function (e) { this.errorHandler(e) }.bind(this);
    }

    private loadHandler(event) {
        var csv = event.target.result;
        this.processData(csv);
    }

    private processData(csv) {
        var allTextLines = csv.split(/\r\n|\n/);
        var lines = [];
        for (var i = 0; i < allTextLines.length; i++) {
            var data = allTextLines[i].split(',');
            var tarr = [];
            for (var j = 0; j < data.length; j++) {
                tarr.push(data[j]);
            }
            lines.push(tarr);
        }
        if (typeof this.onDataProcessed == "function") this.onDataProcessed(lines);
    }

    private errorHandler(evt) {
        if (evt.target.error.name == "NotReadableError") {
            alert("Canno't read file !");
        }
    }
}