import { Component, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import { ApiService } from 'src/app/services/api/api-service.service';
import { VariableService } from 'src/app/services/variable/variable.service';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private apiService: ApiService,
    private variable: VariableService,
    private messageService: MessageService
  ) { }
  ngOnInit(): void {
  }


}
