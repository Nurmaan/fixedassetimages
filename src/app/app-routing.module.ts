import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CreatorComponent } from './components/creator/creator.component';


const routes: Routes = [
    { path: '', redirectTo: '/creator', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'creator', component: CreatorComponent },
    { path: '**', redirectTo: '/creator' }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
