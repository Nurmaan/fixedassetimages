import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MessageService } from '../message/message.service';
import { LoadService } from '../load/load.service';
import { DefaultService } from '../default/default.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  oneTimeToken: string = localStorage.getItem("oneTimeToken");
  userToken: string = localStorage.getItem("userToken");

  constructor(private http: HttpClient, private messageService: MessageService, private router: Router, private loadService: LoadService, private defaultService: DefaultService) { }

  post(url: string, data: Object, param?: Object, raw: boolean = true, requireData?: boolean, defaultHeader: boolean = false, showLoading: boolean = true): Observable<any> {
    if (typeof param == "undefined") param = {};
    if (typeof raw == "undefined") raw = false;
    if (typeof requireData == "undefined") requireData = false;
    const post = new Observable(observer => {
      param = (defaultHeader ? this.getHeader(param) : param);
      var id = this.defaultService.generate(10);
      if (showLoading) this.loadService.loading(true, "api" + id);
      setTimeout(function () {
        this.http.post(url, data, param).subscribe((response) => {
          if (showLoading) this.loadService.loading(false, "api" + id);
          if (raw || this.checkResponse(response, requireData)) {
            observer.next(response);
          } else {
          }
        },
          (error) => {
            if (showLoading) this.loadService.loading(false, "api" + id);
            this.messageService.showError("Oops", "It seems like something went wrong. Hold On while we fix it!");
            observer.next({ success: false, error: error });
          });
      }.bind(this), 100);

    });

    return post;
  }

  get(url: string, param?: Object, raw?: boolean, requireData?: boolean): Observable<any> {
    if (typeof param == "undefined") param = {};
    if (typeof raw == "undefined") raw = true;
    if (typeof requireData == "undefined") requireData = false;
    const get = new Observable(observer => {

      param = this.getHeader(param);

      this.http.get(url, param).subscribe(data => {
        if (raw || this.checkResponse(data, requireData)) {
          observer.next(data);
        } else {
          this.messageService.showError(null, "Session Expired. Please Login Again");
          this.router.navigateByUrl("/login");
        }
      });
    });

    return get;

  }

  private getHeader(param: Object = {}): Object {
    this.oneTimeToken = localStorage.getItem("oneTimeToken");
    this.userToken = localStorage.getItem("userToken");
    if (typeof param['headers'] == "undefined") param['headers'] = {};
    param['headers']['UserToken'] = localStorage.getItem("userToken") || "anonymous";

    return param;
  }

  private checkResponse(response: Object, requireData: boolean): boolean {
    if (response['success']) {
      if (typeof response['data']['userToken'] != "undefined") {
        localStorage.setItem("userToken", response['data']['userToken']);
        localStorage.setItem("name", response['data']['name']);
      }
      return true;
    } else {
      if (response['error'].toLowerCase().includes("invalid session")) {
        this.messageService.showWarning("Session", "Invalid Session");
        this.router.navigateByUrl('/admin/login');
      }
      return false;
    }
  }
}
