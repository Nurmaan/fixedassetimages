import { Injectable } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = [];

  constructor(public toastr: ToastrManager) {
  }
  add(message: string) {
    console.log(message);
    this.messages.push(message);
  }

  display(message: string) {
    this.messages = [];
    this.messages.push(message);
    this.toastr.successToastr(message, null);
  }

  clear() {
    this.messages = [];
  }

  showSuccess(title: string = "Success", message: string) {
    this.toastr.successToastr(message, title);
  }

  showError(title: string = "Oops", message: string) {
    this.toastr.errorToastr(message, title);
  }

  showWarning(title: string = "Alert", message: string) {
    this.toastr.warningToastr(message, title);
  }

  showInfo(title: string = "Info", message: string) {
    this.toastr.infoToastr(message, title);
  }

  showCustom(title: string, message: string) {
    this.toastr.customToastr(title, null, { enableHTML: true });
  }

  showToast(title: string, message: string, position: any = 'top-left') {
    this.toastr.infoToastr(message, title, { position: position });
  }

  showCustom2(title: string, message: string) {
    this.toastr.customToastr('<span style="color: #b50101">' + message + '</span>', null, { enableHTML: true });
  }

}
