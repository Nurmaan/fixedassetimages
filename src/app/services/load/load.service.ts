import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadService {
  constructor() { }
  requests: Object = {};
  loading(active: boolean, name: string) {
    if (active) {
      this.requests[name] = true;
      try {
        document.getElementById("load").classList.add("active");
      } catch (e) { }

    } else {

      this.requests[name] = false;
      var valid = true;
      for (var key in this.requests) {
        if (this.requests[key]) valid = false
      }
      if (valid) {
        try { document.getElementById("load").classList.remove("active"); } catch (e) { }
      }


    }
  }
}
