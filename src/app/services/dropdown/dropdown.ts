import { MessageService } from "../message/message.service";
import { DefaultService } from '../Default/default.service';
import { Injectable } from '@angular/core';
import { InjectorInstance } from '../../app.component';
import { LoadService } from '../load/load.service';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../api/api-service.service';
@Injectable()

export class Dropdown {

    _keydown: BehaviorSubject<Object> = new BehaviorSubject(Object);
    messageService: any;
    defaultService: any;
    mapService: any;
    apiService: any;
    loadService: any;

    getElementInterval = null;
    parentElem;

    ready: boolean = false;

    ID: string = "";

    public onclickFunction;

    constructor(
        private parentElemName,
        private buttonName,
        public names = []) {
        this.messageService = InjectorInstance.get(MessageService);
        this.defaultService = InjectorInstance.get(DefaultService);
        this.apiService = InjectorInstance.get(ApiService);
        this.loadService = InjectorInstance.get(LoadService);

        this.getParentElement(function () {
            this.onReady();
        }.bind(this));

    }

    onReady() {
        this.initVariables();
        this.initFunctions();
    }


    initVariables() {
        this.ID = this.defaultService.generate(5);
    }

    initFunctions() {
        this.createInitialElements();
        this.initListeners();
    }

    createInitialElements() {
        this.parentElem.appendChild(
            this.defaultService.createElement("div", { class: "dropdown" }, [
                this.defaultService.createElement("div",
                    {
                        class: "dropdownButton dropdown-toggle",
                        id: "dropdown_" + this.ID,
                        'data-toggle': "dropdown",
                        'aria-haspopup': true,
                        'aria-expanded': false
                    }, [], { innerHTML: this.buttonName }),
                this.defaultService.createElement("div", { id: "menu_" + this.ID, class: "dropdown-menu", 'aria-labelledby': "dropdown_" + this.buttonName }, [
                    this.defaultService.createElement("input", { type: "search", class: "dropdownInput", id: "search_" + this.ID, placeholder: "Search" }),
                    this.defaultService.createElement("div", { id: "menuItems_" + this.ID }),
                    this.defaultService.createElement("div", { id: "empty_" + this.ID, class: "dropdown-header", style: "display:none;" }, [], { innerHTML: "Nothing Found" })
                ])
            ])
        );
    }

    setNames(names) {
        this.names = names;
        this.buildDropDown(names, true);
    }

    buildDropDown(values, clear = false) {
        if (clear) document.getElementById('menuItems_' + this.ID).innerHTML = "";
        let contents = [];
        for (let name of values) {
            contents.push('<div class="noselect dropdown-item dropdown-item_' + this.ID + '" data-id="' + name['id'] + '" data-name="' + name['description'] + '">' + name['description'] + '</div>');
        }
        $('#menuItems_' + this.ID).append(contents.join(""));

        $('#empty').hide();
    }

    initListeners() {
        $('#menuItems_' + this.ID).on('click', function (e) {
            console.log(e.target);
            if (e.target.id == 'menuItems_' + this.ID) {
                this.messageService.showWarning("Invalid Selection", "You tried to highlight and select multiple items from the dropdown...");
            } else {
                $('#dropdown_' + this.ID).text(e.target.getAttribute("data-name"));
                ($("#dropdown_" + this.ID) as any).dropdown('toggle');
                if (typeof this.onclickFunction == "function") this.onclickFunction(e.target.getAttribute("data-name"), e.target.getAttribute("data-id"));
            }

        }.bind(this));

        this.buildDropDown(this.names);

        $("#search_" + this.ID).keyup(function (e) {
            this.filter(e.target.value);
        }.bind(this))
    }

    filter(word) {
        var items = $(".dropdown-item_" + this.ID);
        let length = items.length
        let collection = []
        let hidden = 0
        for (let i = 0; i < length; i++) {
            if (items[i]['innerHTML'].toLowerCase().includes(word)) {
                $(items[i]).show()
            }
            else {
                $(items[i]).hide()
                hidden++
            }
        }

        //If all items are hidden, show the empty view
        if (hidden === length) {
            $('#empty_' + this.ID).show()
        }
        else {
            $('#empty_' + this.ID).hide()
        }
    }

    getParentElement(callback = null) {// keep checking until parent elem is here
        this.getElementInterval = setInterval(function () {
            this.parentElem = document.getElementById(this.parentElemName);
            if (typeof this.parentElem != "undefined" && this.parentElem != null) {
                clearInterval(this.getElementInterval);
                this.ready = true;
                console.log("Ready");
                if (typeof callback == "function") callback();
            }
        }.bind(this), 500);
    }

    whenReady(callback) {
        let interval = setInterval(function () {
            if (this.ready) {
                clearInterval(interval);
                if (typeof callback == "function") callback();
            }
        }.bind(this), 500);
    }

}
