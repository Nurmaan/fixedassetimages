export class LoginReturnType {
    firstName: string;
    lastName: string;
    userToken: string;
}

export class matchDataServer {
    campus: string;
    discord: string;
    group: string;
    result: number;
    round: string;
    roundID: number;
    groupID: string;
    kill: number;
    death: number;
    time: string;
    userToken: string;
    matchID: string;
    host: number;

    //added
    day: number;
    month: string;
    year: number;
    hour: string;
    epoch: number;
}

export class leaderboardDataServer {
    campus: string;
    firstName: string;
    lastName: string;
    kill: number;
    death: number;
    ktd: string;
    discord: string;
    rank: number;
    status: number;
}

export interface BaseReturnType<T> {
    success: boolean;
    data: T;
    error: string;
    msg: string;
}