import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VariableService {
  //base
  baseApi: string;
  ui: string;

  //derived
  FixedAsset: string;

  constructor() {
  }

  setBaseApi(base) {
    this.baseApi = base;
    this.setVariables();
  }

  setUILink(ui) {
    this.ui = ui;
  }

  setVariables() {
    this.FixedAsset = this.baseApi + "fa";
  }
}
