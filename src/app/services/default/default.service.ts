import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { BehaviorSubject, Observable, bindCallback } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DefaultService {
  _changeHeader: BehaviorSubject<Object> = new BehaviorSubject(Object);

  sidebarActive: boolean = true;

  imageZoomWindow: boolean = false;

  validityRadio = {};
  passwordVisible = {};

  constructor(private router: Router) {
  }

  initLinkListener(arg = null) {
    $("a").click(e => {
      if (!e.target.classList.contains("noAuto")) window.open(e.target.getAttribute("href"), arg);
    });
  }

  bindWindowResize() {
    window.addEventListener("resize", function () {
      this._windowResize.next(document.body.offsetWidth);
    }.bind(this));
  }

  bindMouseMovement() {
    window.addEventListener("mousemove", function (e) {
      this._mousemove.next(e);
    }.bind(this));
    this.bindMouseClick();
    this.bindMouseUp();
    this.bindMouseDown();
  }

  styleDatatable(searchContainer = null) {
    var elems = $(".dataTables_wrapper") as any;
    for (var elem of elems) {
      //style search
      var searchParent = $(elem).find(".dataTables_filter")[0];
      searchContainer = searchContainer == null ? searchParent : searchContainer;
      var input = searchParent.children[0].children[0];
      searchContainer.appendChild(input);
      this.removeElement(searchParent.children[0]);
      input.setAttribute("placeholder", "Search");
      input.classList.add("dropdownInput");
      input.classList.add("bg-transparent");
    }
    ($('[data-toggle="tooltip"]') as any).tooltip();
  }

  bindMouseClick() {
    window.addEventListener("click", function (e) {
      this._mouseclick.next(e);
    }.bind(this));
  }
  bindMouseDown() {
    window.addEventListener("mousedown", function (e) {
      this._mousedown.next(e);
    }.bind(this));
  }
  bindMouseUp() {
    window.addEventListener("mouseup", function (e) {
      this._mouseup.next(e);
    }.bind(this));
  }

  getUrl(): Object {
    return window.location;
  }

  initImages() {
    var initImagesInterval = setInterval(function () {
      var images = document.getElementsByClassName("image");
      if (images.length > 0) {
        for (var i = 0; i < images.length; i++) {
          images[i]["src"] =
            this.variables.imageURL + images[i].getAttribute("data-src");
          (images[i] as any).onerror = function () {
            this.classList.add("d-none");
          }
        }

        window.clearInterval(initImagesInterval as any);
      }

    }.bind(this), 10);

  }

  initPasswordVisibility() {// change password visibility
    $(".passwordVisibility").unbind().click(function (e) {
      var elem = this.getParentByClassName(e.target, "passwordVisibility");
      var id = elem.id;
      var icon = $(elem).find(".passwordIcon")[0];
      var input = $(elem.parentNode).find(".passwordVisibilityInput")[0] as any;
      if (this.passwordVisible[id]) {
        this.passwordVisible[id] = false;
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
        input.type = "password";
      } else {
        this.passwordVisible[id] = true;
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
        input.type = "text";
      }
    }.bind(this));

  }

  initAutoHeight() {
    $(".autoHeight").keyup(e => {
      e.target.style.height = (((e.target as any).textLength * 16) / e.target.offsetWidth) * 20 + "px";
    });
  }

  isNull(obj: any) {
    return typeof obj == "undefined" || typeof obj == null;

  }

  base64ToBlob(b64Data, contentType, sliceSize = 512) {
    console.log("base64");
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  create_number() {
    var min = -1000000;
    var max = -10;
    return Math.floor(Math.random() * (+max - +min)) + +min;
  }

  isArrayEqual(a, b, sort = true) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;
    if (sort) {
      a.sort();
      b.sort();
    }
    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }

  sendLocalstorageMessage(message: Object) {// send a message to all localStorage listeners
    localStorage.setItem('message', JSON.stringify(message));
    localStorage.removeItem('message');
  }

  initPdfZoom() {
    var pdfs = document.getElementsByClassName("pdfName");
    for (var i = 0; i < pdfs.length; i++) {
      (pdfs[i] as HTMLImageElement).onclick = function (event) {
        window.open(event.target[''], "", 'popUpWindow');
      }.bind(this);
    }
  }

  generate(length = 5) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
  }
  redirectToLogin() {
    this.router.navigateByUrl("/login");
  }
  redirectToLoginPole() {
    this.router.navigateByUrl("/loginpole");
  }

  createElement(type, attributes, children = [], properties = {}) {
    var elem = document.createElement(type);
    for (var attribute in attributes) {
      elem.setAttribute(attribute, attributes[attribute]);
    }

    for (var i = 0; i < children.length; i++) {
      if (children[i] != null && typeof children[i] != "undefined") {
        if (typeof children[i]['classList'] != "undefined") {
          elem.appendChild(children[i]);
        } else {
          elem.appendChild(this.createElement(children[i]['type'], children[i]['attributes'], children[i]['children'] || [], children['properties'] || {}));
        }

      };
    }

    for (var property in properties) {
      if (property == "innerHTML") {
        elem[property] += properties[property];
      } else {
        elem[property] = properties[property];
      }
    }

    return elem;
  }



  mergeObject(obj1: Object, obj2: Object): Object {
    for (var key in obj2) {
      obj1[key] = obj2[key];
    }
    return obj1;
  }


  removeElement(elem) {
    try {
      elem.parentNode.removeChild(elem);
    } catch (e) { }

  }

  bubbleSortObject(a) { //expects order in object
    var swapped;
    do {
      swapped = false;
      for (var i = 0; i < a.length - 1; i++) {
        if (a[i]['order'] > a[i + 1]['order']) {
          var temp = a[i];
          a[i] = a[i + 1];
          a[i + 1] = temp;
          swapped = true;
        }
      }
    } while (swapped);
  }

  initCustomCollapse() {
    $(document).ready(function () {
      $(".customCollpaseArrow").click(function (e) {
        if (e.target.classList.contains("active")) {
          console.log(e.target.parentNode.parentNode, $(e.target.parentNode.parentNode), $(e.target.parentNode.parentNode).find(".customCollpaseBody"));
          $(e.target.parentNode.parentNode).find(".customCollpaseBody").removeClass("active");
          $(e.target).removeClass("active");
        } else {
          $(e.target.parentNode.parentNode).find(".customCollpaseBody").addClass("active");
          $(e.target).addClass("active");
        }
      }.bind(this));
    }.bind(this));

  }

  replaceElem(targetElem, replaceWith, input = 1, keepHTML = false) {
    var attributes = this.concatHashToString(targetElem.attributes);
    var replacingStartTag = '<' + replaceWith + attributes + '>';
    var replacingEndTag = '</' + replaceWith + '>';
    var elem = $.parseHTML(replacingStartTag + (keepHTML ? $(targetElem).html() : "") + replacingEndTag)[0];
    var value = (input == 2 ? targetElem.value : targetElem.innerHTML);
    $(targetElem).replaceWith(elem);
    if (input == 1) elem['value'] = value;
    else if (input == 2) elem['innerHTML'] = value;
    return elem;
  }

  concatHashToString(hash) {
    var emptyStr = '';
    $.each(hash, function (index) {
      emptyStr += ' ' + hash[index].name + '="' + hash[index].value + '"';
    });
    return emptyStr;
  }

  getParentByClassName(elem, cls) {
    for (var i = 0; i < 20; i++) {
      if (elem.classList.contains(cls)) return elem;
      else elem = elem.parentNode;
    }

    return false;
  }


  /* --------------------------------- INPUT VALIDATION ------------------------------*/
  validate(parent = null) {
    if (parent == null) parent = document.body;
    var elems = $(parent).find("input, textarea");
    var ret = true;
    var password = "";
    this.clearValidateError();

    for (var i = 0; i < elems.length; i++) {
      var elem = elems[i];
      var validity;
      if (elem.getAttribute("required") != null && (!(validity = this.isInputValid(elem)).result || ((elem as any).value == "" && elem.innerHTML == ""))) {

        ret = false;
        this.displayElementError(parent, elem, validity['msg']);
      }

      if (elem.getAttribute("password") != null) {
        password = (elem as any).value || elem.innerHTML;
      }
      if (elem.getAttribute("password-repeat") != null && password != ((elem as any).value || elem.innerHTML)) {
        this.displayElementError(parent, elem, "Passwords do not match");
        ret = false;
      }
    }
    return ret;
  }

  displayElementError(parent, elem, error = "") {
    $(parent).animate({ scrollTop: $(elem).offset().top }, 100);
    if (elem.getAttribute("error") != null) error = elem.getAttribute("error");
    var id = this.generate(15);
    var newNode = this.createElement("div", { class: "validateError position-relative", id: id }, [], { innerHTML: error });
    this.insertAfter(newNode, elem);
    elem.style.marginBottom = "1em";
    elem.style.border = "1px solid red!important";
  }

  result(parent = document.body) {
    if (this.validate(parent)) {
      var res = {};
      var chosen = [];
      var collections = $(parent).find(".collection");
      var groups = $(parent).find(".group");

      if (collections.length > 0) {
        for (var collection of collections) {
          var innerResult = [];
          $(collection).find(".group").each((k, elem) => {
            var inner2 = {}
            $(elem).find("input,textarea").each((j, innerElem) => {
              inner2[innerElem.getAttribute("result")] = (innerElem as any).value || innerElem.innerHTML;
              chosen.push(innerElem);
            });
            innerResult.push(inner2);
          });
          res[collection.getAttribute("result")] = innerResult;
        }
      } else if (groups.length > 0) {
        for (var group of groups) {
          var name = group.getAttribute('result');
          res[name] = {};
          $(group).find("input,textarea").each((i, elem) => {
            res[name][elem.getAttribute('result')] = elem.innerHTML || (elem as any).value;
            chosen.push(elem);
          })
        }
      }

      $(parent).find("input,textarea,select").each((i, elem) => {
        if (elem.getAttribute("result") != null && chosen.indexOf(elem) == -1) {
          var type = elem['type'];
          if (type == "number" || type == "text" || type == "phone" || type == "email" || type == "textarea" || type == "password" || type == "date" || type == "tel") {
            res[elem.getAttribute("result")] = elem['value'] || elem.innerHTML;
          } else if (type == "radio") {
            if (elem['checked']) res[elem.getAttribute("result")] = elem.getAttribute("value");
          } else if (type == "select-one") {
            res[elem.getAttribute("result")] = elem['options'][elem['selectedIndex']].text;
          }
        }

      });
      return res;
    } else {
      return false;
    }
  }

  isInputValid(input) {
    var type = input.type;
    var value = input.innerHTML || input.value;
    var ret = true;
    var msg = "Required";
    switch (type) {
      case "email":
        if (!value.includes("@") || !value.includes(".")) {
          ret = false
          msg = "Invalid Email Format"
        }
        break;
      case "number":
        if (isNaN(parseInt(value))) {
          ret = false;
          msg = "Invalid Number";
        }
        break;
      case "phone":
        if (isNaN(parseInt(value)) || value > 30 || value < 4) {
          ret = false;
          msg = "Invalid Phone Number | Do not add brackets";
        }
        break;
      case "radio":
        if (typeof this.validityRadio[input.name]) {
          this.validityRadio[input.name] = false;
        }
        if (input.checked) this.validityRadio[input.name] = true;
    }

    if (input.getAttribute("min-length") != null && value.length < input.getAttribute("min-length")) {
      ret = false;
      msg += " Value is too Short (> " + input.getAttribute("min-length") + ")";
    }
    if (input.getAttribute("max-length") != null && value.length > input.getAttribute("max-length")) {
      ret = false;
      msg += " Value is too Long (< " + input.getAttribute("max-length") + ")";
    }
    return { result: ret, msg: msg };
  }

  clearValidateError() {
    var errorElems = document.getElementsByClassName("validateError");
    while (errorElems.length > 0) {
      var errorElem = errorElems[0];
      (errorElem.previousSibling as HTMLElement).style.marginBottom = "auto";
      this.removeElement(errorElem);
    }
  }

  insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
  /* --------------------------------- END INPUT VALIDATION ------------------------------*/

  confirm(title = "ShopShare", message, success, error, buttons = ["Confirm", "Cancel"]) {
    ($('#confirm') as any).modal('show');
    $('#confirm')[0].style.zIndex = "10001";

    //send to foreground
    var modals = document.getElementsByClassName("modal-backdrop fade show");
    (modals[modals.length - 1] as any).style.zIndex = "10000";

    $('#confirmAccept')[0].innerHTML = buttons[0];
    if (buttons.length > 1) {
      $('#confirmClose')[0].innerHTML = buttons[1];
      $('#confirmClose').removeClass("d-none");
    } else {
      $('#confirmClose').addClass("d-none");
    }
    $('#confirmBody')[0].innerHTML = message;
    $('#confirmTitle')[0].innerHTML = title;

    $('#confirmClose').unbind().click(function () {
      if (typeof error == "function") error();
    });
    $('#confirmAccept').unbind().click(function () {
      if (typeof success == "function") success();
    });
  }
}

